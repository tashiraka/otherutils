'''
This script culculates GC content for each read in a fastq file.
Counting the number of {'G', 'C', 'g', 'c', 'S', 's'}.
'N's are NOT considered GC content but included in the length of a read. 
'''

import sys
from Bio import SeqIO, SeqUtils

argvs = sys.argv
argc = len(argvs)

if (argc != 3):
    print 'Usage: # python %s <input fastq filename> <output txt filename>' % argvs[0]
    quit()

fq = SeqIO.parse(argvs[1], 'fastq')
fout = open(argvs[2], 'w')

for record in fq:
	fout.write(str(SeqUtils.GC(record.seq)))
	fout.write('\n')
