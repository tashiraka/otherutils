library(phom)

args <- commandArgs(TRUE)
if(length(args) != 2) {
	stop(paste("Usage: Rscript --slave --vanilla <this script file name> <xyz matrix file> <outfile>"))
}

inFile <- args[1]
outFile <- args[2]

data <- read.table(inFile, sep="\t")
points <- as.matrix(data)
intervals <- pHom(points, 2, 0.5, "vr", "euclidean")

write.table(intervals, outFile)

plotPersistenceDiagram(intervals, 2, 1.0)
plotBarcodeDiagram(intervals, 1, 1.0)
