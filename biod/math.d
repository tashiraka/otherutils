module math;
import std.algorithm, std.math;


double sum(double[] x)
{
  return x.reduce!("a + b");
}


double average(double[] x)
{
  return x.sum / x.length;
}


double innerProduct(double[] x, double[] y)
{
  auto solution = 0.0;
  foreach(i, elem; x) {
    solution += x[i] * y[i];
  }
  return solution;
}


double stdDev(double[] x)
{
  auto solution = 0.0;
  auto avrg = x.average;
  foreach(elem; x) {
    solution += (elem - avrg)^^2;
  }
  return solution.sqrt;
}


double cov(double[] x, double[] y)
{
  auto solution = 0.0;
  auto avrgX = x.average;
  auto avrgY = y.average;
  foreach(i, elem; x) {
    solution += (x[i] - avrgX) * (y[i] - avrgY);
  }
  return solution;
}


double cor(double[] x, double[] y)
{
  return cov(x, y) / (stdDev(x) * stdDev(y));
}


double distEuc(double[] x, double[] y) {
  auto sqrSum = 0.0;
  foreach(i, elem; x) {
    sqrSum += (x[i] - y[i])^^2;
  }
  return sqrSum.sqrt;
}
