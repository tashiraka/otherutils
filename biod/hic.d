import std.stdio, std.algorithm, std.conv;
import math;


// assume a symmetric matrix;
double[][] corMat(double[][] M)
{ 
  double[][] corM;
  corM.length = M.length;
  foreach(i, row; corM) {
    corM[i].length = M.length;
  }

  for(auto i=0; i<M.length; i++) {
    for(auto j=i; j<M.length; j++) {
      corM[i][j] = cor(M[i], M[j]);
      corM[j][i] = corM[i][j];
    }
  }

  return corM;
}


unittest {
  auto M = [[1.0, 2.0],
	    [2.0, 3.0]];
  auto corM = [[1.0, 1.0],
		[1.0, 1.0]];
  assert(to!string(M.corMat) == to!string(corM));
}

