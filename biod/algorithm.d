import std.algorithm, std.conv;


double max(double[] arr)
{
  return arr.reduce!(std.algorithm.max);
}


unittest {
  auto x = [1.0, 2.0, 3.0];
  assert(x.max == 3.0, x.max.to!string);
}


double min(double[] arr)
{
  return arr.reduce!(std.algorithm.min);
}


unittest {
  auto x = [1.0, 2.0, 3.0];
  assert(x.min == 1.0, x.min.to!string);
}
