import io, hic;

version(unittest){}
 else {
   void main(string[] args)
   {
     assert(args.length == 3,
	    "Usage: " ~ args[0] ~ " <contact matrix file> <output file>");

     auto inFile = args[1];
     auto outFile = args[2];

     readMat(inFile).corMat.writeMat(outFile);
   }
 }
