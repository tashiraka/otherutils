module io;
import std.stdio, std.array, std.conv;


double[][] readMat(string filename)
{
  auto app = appender!(double[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    app.put(line.to!string.split.to!(double[]));
  }
  return app.data;
}


unittest {
  import std.file;
  auto deleteme = testFilename();
  auto fout = File(deleteme, "w");
  fout.write("1.0\t2.2\t3.0\n4.0\t5.5\t6.0\n");
  fout.close();
  scope(exit) remove(deleteme);

  auto M = [[1.0, 2.2, 3.0],
	    [4.0, 5.5, 6.0]];
  auto readM = readMat(deleteme);
  assert(M == readM, "assert(M == readM) : "
	 ~ to!string(M) ~ " vs " ~ to!string(readM));
}


void writeMat(double[][] M, string filename)
{
  auto fout = File(filename, "w");
  foreach(row; M) {
    foreach(i, elem; row) {
      fout.write(elem);
      if(i != row.length - 1) {
	fout.write('\t');
      }
    }
    fout.writeln;
  }
}


void write3DMat(double[][][] M, string filename)
{
  auto fout = File(filename, "w");
  foreach(i, yz; M) {
    fout.writeln('>' ~ i.to!string);
    foreach(z; yz) {
      foreach(j, elem; z) {
	fout.write(elem);
	if(j != z.length - 1) {
	  fout.write('\t');
	}
      }
      fout.writeln;
    }
  }
}


double[][] readPDB(string filename) 
{
  auto app = appender!(double[][]);
  auto fin = File(filename, "r");
  foreach(line; fin.byLine) {
    if(line.length >= 6 && line[0..6] == "ATOM  ") {
      auto x = line[30..38].to!string.split(' ');
      auto y = line[38..46].to!string.split(' ');
      auto z = line[46..54].to!string.split(' ');
      app.put([x[x.length -1].to!double,
	       y[y.length -1].to!double,
	       z[z.length -1].to!double]);
    }
  }
  return app.data;
}


unittest {
  import std.file;
  auto deleteme = testFilename();
  auto fout = File(deleteme, "w");
  auto pdb = "AUTHOR    ZHANG ZHIZHUO and Yuichi Motai
ATOM      1 C    LIG A          62.212  40.572  59.309  1.00 75.00    
ATOM      2 C    LIG A          86.114  24.331  82.892  1.00 75.00    
ATOM      3 C    LIG A          65.439  32.056  58.841  1.00 75.00    
ATOM      4 C    LIG A          74.460  30.483  65.701  1.00 75.00    
ATOM      5 C    LIG A          91.734   9.743  67.837  1.00 75.00    
END";
  fout.write(pdb);
  fout.close();
  scope(exit) remove(deleteme);

  auto pos = [[62.212, 40.572, 59.309],
	      [86.114, 24.331, 82.892],
	      [65.439, 32.056, 58.841],
	      [74.460, 30.483, 65.701],
	      [91.734, 9.743, 67.837]];
  auto readPos = readPDB(deleteme);
  assert(pos == readPos, "assert(pos == readPos) : "
	 ~ to!string(pos) ~ " vs " ~ to!string(readPos));
}

/*
void writeTiff(bool[][][] occ, string filename)
{
  struct Header {
    auto byteOrder = "MM";
    auto ver = ".";
    auto IDFZeroOffset = "*";
    string toString() {
      return byteOrder ~ ver ~ IDFZeroOffset;
    }
  }
  struct IFD {
    string numDirEntry;
    string[] tagList;
    string nextIFDOffset;
  }

  auto fout = File(filename, "w");
  Header h;
  IFD[] ifd;
  fout.write(h.toString);
  fout.write();
}
*/

version(unittest) string testFilename(string file = __FILE__, size_t line = __LINE__)
{
    import std.conv : text;
    import std.path : baseName;

    return text("deleteme-.", baseName(file), ".", line);
}
