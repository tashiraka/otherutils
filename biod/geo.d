module geo;
import std.array, std.algorithm, std.math, std.conv, std.stdio;
import math, algorithm;


double[] distBetweenVertex(double[][] pos)
{
  auto app = appender!(double[]);
  double[] prev;
  foreach(i, vertex; pos) {
    if(i != 0) {
      app.put(distEuc(vertex, prev));
    }
    prev = vertex;
  }
  return app.data;
}


unittest {
  auto x = [[0.0, 0.0, 0.0],
	    [1.0, 1.0, 1.0],
	    [3.0, 3.0, 2.0]];
  auto dist = x.distBetweenVertex;
  assert(dist == [sqrt(3.0), 3.0],
	 "dist == [sqrt(3.0), 3.0] : " ~ dist.to!string);
}


double[][] interpolateByBoxelSize(double[][] pos, double boxelSize)
{
  auto app = appender!(double[][]);
  foreach(i, vertex; pos) {
    if(i != 0) {
      auto dist = distEuc(pos[i-1], pos[i]);
      auto sepNum = cast(int)(dist / boxelSize);
      for(auto j=1; j<=sepNum; j++) {
	app.put([pos[i-1][0] + (pos[i][0] - pos[i-1][0]) * j / (sepNum + 1),
		 pos[i-1][1] + (pos[i][1] - pos[i-1][1]) * j / (sepNum + 1),
		 pos[i-1][2] + (pos[i][2] - pos[i-1][2]) * j / (sepNum + 1)]);
      }
    }
    app.put(vertex);
  }
  return app.data;
}


unittest {
  auto x = [[0.0, 0.0, 0.0],
	    [1.0, 1.0, 1.0],
	    [3.0, 3.0, 2.0]];
  auto boxelSize = 1.0;
  auto y = x.interpolateByBoxelSize(boxelSize);
  assert(y == [[0.0, 0.0, 0.0],
	       [0.5, 0.5, 0.5],
	       [1.0, 1.0, 1.0],
	       [1.0 + 2.0 / 4, 1.0 + 2.0 / 4, 1.0 + 1.0 / 4],
	       [1.0 + 4.0 / 4, 1.0 + 4.0 / 4, 1.0 + 2.0 / 4],
	       [1.0 + 6.0 / 4, 1.0 + 6.0 / 4, 1.0 + 3.0 / 4],
	       [3.0, 3.0, 2.0]],
	 y.to!string);
}


class SpaceLattice
{
  double[3] lowerBound;
  double[3] upperBound;
  double latticeSize;
  int[3] size;
  
  this(double[][] pos, double voxelSize)
  {
    auto minPos = [double.max, double.max, double.max];
    auto maxPos = [-double.max, -double.max, -double.max];

    foreach(i, p; pos) {
        for(auto j=0; j<3; j++) {
          minPos[j] = min(minPos[j], p[j]);
          maxPos[j] = max(maxPos[j], p[j]);
      }
    }

    auto range = [maxPos[0] - minPos[0],
		  maxPos[1] - minPos[1],
		  maxPos[2] - minPos[2]];

    minPos = [minPos[0] - range[0] / 10,
	      minPos[1] - range[1] / 10,
	      minPos[2] - range[2] / 10];
    maxPos = [maxPos[0] + range[0] / 10,
	      maxPos[1] + range[1] / 10,
	      maxPos[2] + range[2] / 10];
    
    lowerBound = minPos;
    latticeSize = voxelSize;
    size = [cast(int)((maxPos[0] - minPos[0]) / voxelSize) + 1,
	    cast(int)((maxPos[1] - minPos[1]) / voxelSize) + 1,
	    cast(int)((maxPos[2] - minPos[2]) / voxelSize) + 1];
    
    upperBound = [minPos[0] + voxelSize * size[0],
		  minPos[1] + voxelSize * size[1],
		  minPos[2] + voxelSize * size[2]];
  }
  
  @property int xSize() {
    return size[0];
  }
  @property int ySize() {
    return size[1];
  }
  @property int zSize() {
    return size[2];
  }
  double xLower(int i) {
    assert(0 <= i && i < size[0]);
    return lowerBound[0] + i * latticeSize;
  }
  double yLower(int i) {
    assert(0 <= i && i < size[1]);
    return lowerBound[1] + i * latticeSize;
  }
  double zLower(int i) {
    assert(0 <= i && i < size[1]);
    return lowerBound[2] + i * latticeSize;
  }
  double xUpper(int i) {
    assert(0 <= i && i < size[0]);
    return lowerBound[0] + (i + 1) * latticeSize;
  }
  double yUpper(int i) {
    assert(0 <= i && i < size[1]);
    return lowerBound[1] + (i + 1) * latticeSize;
  }
  double zUpper(int i) {
    assert(0 <= i && i < size[2]);
    return lowerBound[2] + (i + 1) * latticeSize;
  }
  int[3] where(double[] p) {
    assert(p.length == 3);
    return [cast(int)((p[0] - lowerBound[0]) / latticeSize),
	    cast(int)((p[1] - lowerBound[1]) / latticeSize),
	    cast(int)((p[2] - lowerBound[2]) / latticeSize)];
  }
}


unittest {
    auto x = [[0.0, 0.0, 0.0],
	      [0.5, 0.5, 0.5],
	      [1.0, 1.0, 1.0],
	      [1.0 + 2.0 / 4, 1.0 + 2.0 / 4, 1.0 + 1.0 / 4],
	      [1.0 + 4.0 / 4, 1.0 + 4.0 / 4, 1.0 + 2.0 / 4],
	      [1.0 + 6.0 / 4, 1.0 + 6.0 / 4, 1.0 + 3.0 / 4],
	      [3.0, 3.0, 2.0]];
    auto boxelSize = 1.0;
    auto lat = new SpaceLattice(x, boxelSize);

    assert(lat.xSize == 4, lat.xSize.to!string);
    assert(lat.ySize == 4, lat.ySize.to!string);
    assert(lat.zSize == 3, lat.zSize.to!string);
    
    assert(lat.xLower(0) == -0.3, lat.xLower(0).to!string);
    assert(lat.yLower(0) == -0.3, lat.yLower(0).to!string);
    assert(lat.zLower(0) == -0.2, lat.zLower(0).to!string);

    assert(lat.xUpper(lat.xSize-1) == 3.7, lat.xUpper(lat.xSize-1).to!string);
    assert(lat.yUpper(lat.ySize-1) == 3.7, lat.yUpper(lat.ySize-1).to!string);
    assert(lat.zUpper(lat.zSize-1) == 2.8, lat.zUpper(lat.zSize-1).to!string);

    assert(lat.where([0.0, 0.0, 0.0]) == [0, 0, 0]);
    assert(lat.where([1.0, 1.0, 1.0]) == [1, 1, 1]);
    assert(lat.where([3.0, 3.0, 2.0]) == [3, 3, 2]);
}


double[][][] occupancy(SpaceLattice lat, double[][] pos)
{
  auto occ = new double[][][](lat.xSize, lat.ySize, lat.zSize);
  foreach(i, yz; occ)
    foreach(j, z; yz)
      foreach(k, elem; z)
	occ[i][j][k] = 0.0;
  
  foreach(p; pos) {
    auto id = lat.where(p);
    occ[id[0]][id[1]][id[2]] = 1.0;
  }
  
  return occ;
}


unittest {
      auto x = [[0.0, 0.0, 0.0],
	      [0.5, 0.5, 0.5],
	      [1.0, 1.0, 1.0],
	      [1.0 + 2.0 / 4, 1.0 + 2.0 / 4, 1.0 + 1.0 / 4],
	      [1.0 + 4.0 / 4, 1.0 + 4.0 / 4, 1.0 + 2.0 / 4],
	      [1.0 + 6.0 / 4, 1.0 + 6.0 / 4, 1.0 + 3.0 / 4],
	      [3.0, 3.0, 2.0]];
    auto boxelSize = 1.0;
    auto lat = new SpaceLattice(x, boxelSize);
    auto occ = lat.occupancy(x);

    assert(occ[0][0][0] == 1.0);
    assert(occ[1][1][1] == 1.0);
    assert(occ[3][3][2] == 1.0);
}
