import geo, io;
import std.stdio, std.conv;

version(unittest) {}
 else {
   void main(string[] args)
   {
     if(args.length != 4) {
       stderr.writeln("Error: Usage ./" ~ args[0] ~
		      "<pdb file> <lattice size> <out file>");
       return;
     }

     auto pdbFile = args[1];
     auto latticeSize = args[2].to!double;
     auto outFile = args[3];

     auto pos = pdbFile.readPDB;
     pos = pos.interpolateByBoxelSize(latticeSize);
     auto lat = new SpaceLattice(pos, latticeSize);
     auto occ = lat.occupancy(pos);

     occ.write3DMat(outFile);
   }
 }
