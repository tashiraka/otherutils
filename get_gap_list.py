'''
The fields of gap format:
chromosome_name\tgap_start\tgap_end\tgap_length\n

Note:
The coodinate system is 0-origin.
The gap is [gap_start, gap_end).
'''

import sys
from Bio import Seq, SeqIO

args = sys.argv
if(len(args) != 3):
    print "ERROR: Usage # python %s <fasta> <out file>" % args[0]
    quit

fasta_file = args[1]
out_file = args[2]


fout = open(out_file, 'w')

for chromosome in SeqIO.parse(open(fasta_file, 'r'), 'fasta'):
    gap_start = []
    gap_end = []

    if(chromosome.seq[0] == 'N'):
        gap_start += [0]

    for i in range(0, len(chromosome.seq)-1):
        if(chromosome.seq[i] != 'N' and chromosome.seq[i+1] == 'N'):
            gap_start += [i+1]
        if(chromosome.seq[i] == 'N' and chromosome.seq[i+1] != 'N'):
            gap_end += [i+1]

    if(chromosome.seq[-1] == 'N'):
        gap_end += [len(chromosome.seq)]

    if(len(gap_start) != len(gap_end)):
        print 'ERROR: the lengthes of gap_start and gap_end are different.'    
        quit
        
    for i, start in enumerate(gap_start):
        fout.write(chromosome.name + '\t' + str(start) + '\t' + str(gap_end[i]) + '\t' + str(gap_end[i] - start) + '\n')
