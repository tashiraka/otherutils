# filter out wrong mapped reads from a sam file
echo "input the number of reads: "
samtools view -c R1.bam

echo "removing unmapped reads (which do not have a valid alignment)..."
samtools view -bh -F 0x04 R1.bam > tmp.bam

echo "the number of mapped (valid-alignment) reads: "
samtools view -c tmp.bam

echo "removing secondary-mapped reads..."
samtools view -bh -F 0x100 tmp.bam > R1.mapped.bam

echo "the number of max-scored mapped reads: "
samtools view -c R1.mapped.bam

echo "removing low-MAPQ(<10) reads..."
samtools view -bh -q 10 R1.mapped.bam > R1.mapped.unique.bam
