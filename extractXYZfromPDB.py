import sys

args = sys.argv
if(len(args) != 3):
	print "USAGE: python %s <input pdb file> <output XYZ file>" % args[0]
	quit

inFile = args[1]
outFile = args[2]

fout = open(outFile, 'w')
for line in open(inFile, 'r'):
	if(line[0:4] == 'ATOM'):
		x = line[30:38].replace(' ', '')
		y = line[38:46].replace(' ', '')
		z = line[46:54].replace(' ', '')
		fout.write(x + '\t' + y + '\t' + z + '\n')
