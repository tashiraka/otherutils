# For Hi-C data, the concordant flag(0x2) does not have mean.
# The flags 0x200, 0x400 and 0x800 are probably not used for Bowtie2 aligner. 

# A pair is "correct" when the pair are mapped to reference genome in fr or rf.

echo "convert the input sam file to bam format."
samtools view -bhS M.sam -o M.bam

echo "filtering out paired reads which have unmapped(0x4) or pair-unmapped(0x8),\
and secondary-alignmnet(0x100), and are not paied(0x1)"
# 0x1 = 1
samtools view -bh -f 1 M.bam -o tmp.bam
# 0x4 + 0x8 + 0x100 = 268
samtools view -bh -F 268 tmp.bam -o tmp2.bam
rm tmp.bam

echo "firtering out Forward-Forward and Revease-Revease paires.(0x10 and 0x20)"
samtools view -bh -f 16 -F 32 tmp2.bam -0 tmp3.bam
samtools view -bh -f 32 -F 16 tmp2.bam -0 tmp4.bam
rm tmp2.bam
samtools merge -n tmp3.bam tmp4.bam correctOrientedM.bam
rm tmp3.bam tmp4.bam

