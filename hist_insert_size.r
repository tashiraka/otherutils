data = read.table("insert_size")
pdf("hist_insert_size.pdf")
hist(log10(data[,1]), breaks="FD", main="the histgram of the sum of two distances (d1,d2)\nbetween each part of a pair tags and each nearest restriction cut site", xlab="log10(d1 + d2)", col="#0068b7", border="#0068b7")
abline(v=log10(300), lty=2)

