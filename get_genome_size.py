from Bio import SeqIO
import sys

args = sys.argv
if len(args) != 2:
	print "Usage: # python %s <input filename>" % args[0] 
	quit()

handle = open(args[1], 'r')

sum = 0
for chromosome in SeqIO.parse(handle, "fasta") :
	length = len(chromosome.seq)
	sum += length
	print chromosome.name+"\t"+str(length)

print(str(sum))
