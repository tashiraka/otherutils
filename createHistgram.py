'''
This script draws a histgram.
'''
import sys
import pylab as pl


argvs = sys.argv
argc = len(argvs)

if (argc != 2):
	print 'Usage: # python %s <filename>' % argvs[0]
	quit()

GCcontents = open(argvs[1], 'r').read().split('\n')

if (GCcontents[-1] == ''):
	del GCcontents[-1]

GCcontents = map(lambda x: float(x), GCcontents)

pl.hist(GCcontents, normed=True)
pl.show()
