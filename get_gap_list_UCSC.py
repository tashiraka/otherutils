'''
The fields of gap format:
bin\tchrom\tchromStart\tchromEnd\ix\tn\tsize\ttype\tbridge

Note:
field example SQL type info description
bin 585 smallint(6) range Indexing field to speed chromosome range queries.
chrom chr1 varchar(255) values Reference sequence chromosome or scaffold
chromStart 0 int(10) unsigned range start position in chromosome
chromEnd 10000 int(10) unsigned range end position in chromosome
ix 1 int(11) range index count of this fragment (obsolete/useless)
n N char(1) values 'N' for gaps of known size, 'U' for gaps of unknown size
size 10000 int(10) unsigned range size of gap
type telomere varchar(255) values scaffold, contig, clone, fragment, etc.
bridge no varchar(255) values yes, no, mrna, bacEndPair, etc.
'''

import sys
from Bio import Seq, SeqIO

args = sys.argv
if(len(args) != 3):
    print "ERROR: Usage # python %s <fasta> <out file>" % args[0]
    quit

fasta_file = args[1]
out_file = args[2]


fout = open(out_file, 'w')

for chromosome in SeqIO.parse(open(fasta_file, 'r'), 'fasta'):
    gap_start = []
    gap_end = []

    if(chromosome.seq[0] == 'N'):
        gap_start += [0]

    for i in range(0, len(chromosome.seq)-1):
        if(chromosome.seq[i] != 'N' and chromosome.seq[i+1] == 'N'):
            gap_start += [i+1]
        if(chromosome.seq[i] == 'N' and chromosome.seq[i+1] != 'N'):
            gap_end += [i+1]

    if(chromosome.seq[-1] == 'N'):
        gap_end += [len(chromosome.seq)]

    if(len(gap_start) != len(gap_end)):
        print 'ERROR: the lengthes of gap_start and gap_end are different.'    
        quit
        
    for i, start in enumerate(gap_start):
        fout.write('0\t' + chromosome.name + '\t' + str(start) + '\t' + str(gap_end[i]) + '\t0\tN\t' + str(gap_end[i] - start) + '\tscaffold\tno\n')
