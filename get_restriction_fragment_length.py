'''
This script only for oryLat2 .
This script calculates the histgram of restriction fragments 
in oryLat2 by hindIII. 
'''
from Bio import SeqIO, Restriction, Seq
import numpy as np
import sys

args = sys.argv
if(len(args) != 3):
	print 'Usage: # python %s <genome fasta> <out file>' % args[0]
	quit()

genome_file = args[1]
out_file = args[2]

genomeHandle = open(genome_file, "r")

restrictionFragmentLength = []

for chromosome in SeqIO.parse(genomeHandle, "fasta") :
	restrictionSite = Restriction.HindIII.search(chromosome.seq)
	restrictionSite += [len(chromosome.seq) + 1]
	
	prevSite = 1
	for rSite in restrictionSite :
		restrictionFragmentLength += [rSite - prevSite]
		prevSite = rSite

genomeHandle.close()


f = open(out_file, "w")

for len in restrictionFragmentLength :
	f.write(str(len))
	f.write('\n')

f.close()
