'''
This script only for oryLat2 .
'''
import sys
from Bio import SeqIO, Restriction, Seq

argvs = sys.argv
argc = len(argvs)

if (argc != 2):
	print 'Usage: # python %s <filename>' % argvs[0]
	quit()

handle = open(argvs[1], "r")

sum = 0
for chromosome in SeqIO.parse(handle, "fasta") :
	sum += len(Restriction.HindIII.search(chromosome.seq))

print(str(sum))