import sys

args = sys.argv
if(len(args) != 4):
	print "Usage: # python %s <pdb file> <gap list file> <out pdb file>" % args[0]
	quit()

pdb_file = args[1]
gap_list_file = args[2]
out_file = args[3]

binned_wig = map(lambda x: float(x) * 100.0, open(binned_wig_file, 'r').read().rstrip().split('\n'))

pdb_fin = open(pdb_file, 'r')
fout = open(out_file, 'w')

fout.write(pdb_fin.readline())
i = 0
for line in pdb_fin:
	if(line[0] == 'A'):
		
		fout.write(line.rstrip()[0:60] + ' ' + str(binned_wig[i])[0:5] + '\n')
		i += 1
	else:
		fout.write(line)
