import numpy as np
import numpy.random
import matplotlib.pyplot as plt
import sys, math


args = sys.argv
if (len(args) != 3) :
	print "Usage: # python %s <X.cbin> <X.n_contact>" % args[0]
	quit()

cbins = args[1]
n_contact = args[2]


bin_num = -1
for line in open(cbins, 'r'):
	bin_num += 1

first_line = True
max_val = float('-inf')
min_val = float('inf')

for line in open(n_contact, 'r') :
	if (first_line) :
		first_line = False
		continue

	fields = line.split('\t')
	expected_count = float(fields[2])
	observed_count = float(fields[3])

	if (expected_count == 0.0) :
		print 'Error : expected_count is 0'
		quit()

	if (observed_count != 0.0) :
		log_ratio = math.log10(observed_count / expected_count)
		max_val = max(max_val, log_ratio)
		min_val = min(min_val, log_ratio)

contact_matrix = [[min_val for colums in range(bin_num)] for row in range(bin_num)]
first_line = True
for line in open(n_contact, 'r') :
	if (first_line) :
		first_line = False
		continue

	fields = line.split('\t')
	cbin1 = int(fields[0])
	cbin2 = int(fields[1])
	expected_count = float(fields[2])
	observed_count = float(fields[3])

	log_ratio = None
	if (observed_count != 0.0) :
		log_ratio = log_ratio = math.log10(observed_count / expected_count)
	elif (observed_count == 0.0) :
		log_ratio = min_val

	contact_matrix[cbin1 - 1][cbin2 - 1] = log_ratio
	contact_matrix[cbin2 - 1][cbin1 - 1] = log_ratio



# create heatmap
fig, ax = plt.subplots()
cax = ax.imshow(contact_matrix, cmap=plt.cm.seismic, interpolation='none', vmax=max_val, vmin=min_val)
ax.set_title('HdRr st. 25 whole embryo (2014Apr15)')
cbar = fig.colorbar(cax)
cbar.set_label('log10(observed_count / expected_count)', rotation=270, labelpad=1)
#cbar.ax.set_yticklabels([-5.0, 0.0, 5.0])
#cbar.ax.set_yticklabels([str(round(min_val, 2)), 0.0, str(round(max_val,2))])
ax.set_xlabel('bins [Mbp]')
ax.set_ylabel('bins [Mbp]')
plt.show()
